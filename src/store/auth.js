const state = {
  accessToken: ''
}

const getters = {
}

const mutations = {
  SET_ACCESS_TOKEN(state, token) {
    state.accessToken = token
  }
}

const actions = {
  login: async () => {
    
  },

  logout: async () => {

  },

  setAccessTopken({commit}, token) {
    commit('SET_ACCESS_TOKEN', token)
  }
}

const module = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}

export default module