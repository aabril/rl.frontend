import Home from '@/views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('@/views/About.vue')
  },
  {
    path: '/callback',
    name: 'callback',
    component: () => import('@/views/Callback.vue')
  }
]

export default routes