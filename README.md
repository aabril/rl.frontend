# RL Frontend

This project will try to showcase the use of the Spotify API using VueJS.
Not sure, what I am going to implement yet, yet I found this screenshot on Instagram that could help me as inspiration
https://www.instagram.com/p/Bn0hEa1ncIC/
https://dribbble.com/shots/5241654-Mac-Miller-Concept-RIP

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```
